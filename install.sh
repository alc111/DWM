#!/bin/bash
read -p "Introduce tu usuario:" user
read -p "Introduce tu distribución(Gentoo/gentoo, Arch/arch o Ubuntu/ubuntu):" distro
if [ $distro == "Gentoo" ] || [ $distro == "gentoo" ]
then
sudo emerge --ask dev-vcs/git media-libs/fontconfig x11-base/xorg-proto x11-libs/libX11 x11-libs/libXft x11-libs/libXinerama x11-terms/alacritty media-fonts/jetbrains-mono media-fonts/fontawesome media-gfx/feh x11-apps/xsetroot x11-apps/setxkbmap media-fonts/takao-fonts media-sound/pnmixer  gnome-extra/nm-applet sys-devel/gcc app-text/asciidoc dev-lang/python dev-libs/libconfig dev-libs/libev dev-libs/libpcre dev-libs/uthash dev-python/xcffib  dev-util/meson dev-util/meson-format-array dev-util/ninja sys-apps/dbus virtual/opengl virtual/pkgconfig x11-apps/xhost x11-base/xorg-server x11-libs/libXext x11-libs/libdrm x11-libs/libxcb x11-libs/pixman x11-libs/xcb-util-image x11-libs/xcb-util-renderutil x11-misc/dunst xfce-base/thunar xfce-extra/xarchiver xfce-extra/thunar-archive-plugin x11-misc/rofi x11-misc/stalonetray
elif [ $distro == "Arch" ] || [ $distro == "arch" ]
then
  sudo pacman -S git fontconfig xorgproto libx11 libxft libxinerama alacritty ttf-jetbrains-mono ttf-font-awesome feh xorg-xsetroot xorg-setxkbmap network-manager-applet dunst thunar xarchiver thunar-archive-plugin rofi
  yay -S otf-takao volctl stalonetray
elif [ $distro == "Ubuntu" ] || [ $distro == "ubuntu" ]
then
  sudo apt install git dunst thunar rofi nm-tray fonts-takao libxcb-render-util0-dev libxcb-image0-dev libpixman-1-dev libxcb-util-dev libxcb-damage0-dev libxcb-randr0-dev libxcb-sync-dev libxcb-composite0-dev libxcb-xinerama0-dev libxcb-present-dev libxcb-glx0-dev libegl1-mesa-dev libdbus-glib-1-dev libdrm-dev libxext-dev x11-xserver-utils pkg-config libgl-dev dbus ninja-build meson python3-xcffib uthash-dev libpcre3 libpcre3-dev libev-dev libconfig-dev asciidoc python3 gcc pnmixer feh fonts-font-awesome libxinerama-dev libxft-dev libx11-dev fontconfig xorg xserver-xorg x11proto-dev wget libx11-xcb-dev xarchiver thunar-archive-plugin stalonetray
  sudo add-apt-repository ppa:aslatter/ppa
  sudo apt install alacritty
  wget https://download.jetbrains.com/fonts/JetBrainsMono-2.242.zip
  unzip JetBrainsMono-2.242.zip
  cd fonts
  sudo mv ttf /usr/share/fonts/jetbrains-mono
  sudo fc-cache -f -v
  cd ..
else
  echo "Las únicas opciones válidas son Gentoo/gentoo, Arch/arch o Ubuntu/ubuntu."
  exit 1
fi
read -p "Desea instalar el picom de jonaburg o de Ft-labs?(Jonaburg/jonaburg o Labs/labs)" picom
if [ $picom == "Jonaburg" ] || [ $picom == "jonaburg" ]
then
  if [ $distro == "Gentoo" ] || [ $distro == "gentoo" ] || [ $distro == "Ubuntu" ] || [ $distro == "ubuntu" ]
  then
    cd ..
    git clone https://github.com/jonaburg/picom.git
    cd picom
    meson --buildtype=release . build
    ninja -C build
    sudo ninja -C build install
    cd ../DWM
  else
    yay -S picom-jonaburg-git
  fi
  mkdir -p /home/$user/.config/picom
  cp dotfiles/picom/picom-jonaburg.conf /home/$user/.config/picom/picom.conf
else
  if [ $distro == "Gentoo" ] || [ $distro == "gentoo" ] || [ $distro == "Ubuntu" ] || [ $distro == "ubuntu" ]
  then
    cd ..
    git clone https://github.com/FT-Labs/picom.git
    cd picom
    git submodule update --init --recursive
    meson --buildtype=release . build
    ninja -C build
    sudo ninja -C build install
    cd ../DWM
  else
    yay -S picom-ftlabs-git
  fi
  mkdir -p /home/$user/.config/picom
  cp dotfiles/picom/picom-labs.conf /home/$user/.config/picom/picom.conf
fi
read -p "Desea instalar los launchers de adi1090x?(Si/si o No/no):" launcher
if [ $launcher == "Si" ] || [ $launcher == "si" ]
then
  cd ..
  git clone --depth=1 https://github.com/adi1090x/rofi.git
  cd rofi
  chmod +x setup.sh
  ./setup.sh
  cd ../DWM
  sudo cp dotfiles/launcher/launcher.sh /usr/local/bin
  sudo chmod +x /usr/local/bin/launcher.sh
fi
##Configurando dunst
mkdir -p /home/$user/.config/dunst
cd dotfiles/dunst
cp * /home/$user/.config/dunst
##Configurando el powermenu
cd ../powermenu
mkdir -p /home/$user/.config/rofi
cp powermenu.rasi /home/$user/.config/rofi
sudo cp powermenu /usr/local/bin
sudo chmod +x /usr/local/bin/powermenu
##Configurando stalonetray
cd ../stalonetray
cp stalonetrayrc /home/$user/.config
##DWM y DMENU
cd ../../myDwm
sudo make clean install
cd ../myDmenu
sudo make clean install
cd ..
##Instalación de archivos de inicio de DWM
sudo cp dotfiles/dwminit/dwmstart /usr/local/bin
sudo cp dotfiles/dwminit/dwm.desktop /usr/share/xsessions
sudo cp dotfiles/dwminit/statusbar.sh /usr/local/bin
read -p "Para terminar, desearía instalar temas de vinceliuice/grub2-themes?(Si/si o No/no):" grub
if [ $grub == "Si" ] || [ $grub == "si" ]
then
  cd ..
  git clone https://github.com/vinceliuice/grub2-themes.git
  cd grub2-themes
  read -p "Introduce el tema(tela/vimix/stylish/whitesur):" tema
  read -p "Introduce los iconos(color/white/whitesur):" iconos
  read -p "Introduce tu resolución de pantalla(1080p/2k/4k/ultrawide/ultrawide2k):" resolucion
  sudo ./install.sh -t $tema -i $iconos -s $resolucion
fi
